# -*- coding: utf-8 -*-
{
    'name': "odoo_debranding",

    'summary': """
        odoo debranding """,

    'description': """
        remove powered by Odoo, remove about and my Odoo.com support on user menu etc...
    """,

    'author': "dfang",
    'website': "twitter.com/df1228",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web'],

    # always loaded
    'data': [
        'views/views.xml',
        'views/templates.xml',
        'data/debranding_data.xml',
        'debranding.xml'
    ],
    'qweb' : [
        "static/src/xml/*.xml"
    ],
    # only loaded in demonstration mode
    'demo': [
        'data/demo.xml',
    ],
    'auto_install': True
}
