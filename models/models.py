# -*- coding: utf-8 -*-

from openerp import models, fields, api

class debranding(models.Model):
    _name = 'debranding.debranding'

    # name = fields.Char()
    # value = fields.Integer()
    # value2 = fields.Float(compute="_value_pc", store=True)
    # description = fields.Text()

    # @api.depends('value')
    # def _value_pc(self):
    #     self.value2 = float(self.value) / 100





class ResCompany(models.Model):
    _inherit = "res.company"

    @api.model
    def create(self, vals):
        inherit_id = vals.get('inherit_id', False)
        if inherit_id:
            # this is an inheritance
            del vals['inherit_id']
            super(ResCompany, self.browse(inherit_id)).write(vals)
            return self
        return super(ResCompany, self).create(vals) 


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.model
    def create(self, vals):
        inherit_id = vals.get('inherit_id', False)
        if inherit_id:
            # this is an inheritance
            del vals['inherit_id']
            super(ResPartner, self.browse(inherit_id)).write(vals)
            return self
        return super(ResPartner, self).create(vals) 

    # @api.model
    # def set_my_company_data(self):
    #     _logger.info("Setting company data...")
    #     module_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    #     img_path = os.path.join(module_dir, 'static/img/custom_company_logo.png')
    #     with open(img_path) as img:
    #         logo = img.read().encode('base64')

    #     self.browse(1).write({
    #         'name': "My Custom Name",
    #         'logo': logo,
    #         [...]
    #     })
    #     _logger.info("... done.")  

