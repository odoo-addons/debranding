// odoo.debranding = function(instance, local) {
//     "use strict";
//     // var core = require('web.core');
//     instance.web.UserMenu.Class.include({
//     	on_menu_documentation: function () {
//     			window.open('http://www.odoo.com/documentation/10.0/index.html', '_blank');
//     	}
//     });
//
// }


// http://www.odoo.com/documentation/10.0/howtos/web.html#modify-existing-widgets-and-classes
odoo.define('debranding.UserMenu', function (require) {
  "use strict";
  var UserMenu = require('web.UserMenu');
  UserMenu.include({
  	on_menu_documentation: function () {
  			window.open('http://www.odoo.com/documentation/10.0/index.html', '_blank');
  	}
  });

})
